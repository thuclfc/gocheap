$(document).ready(function () {
   //show input search
   $('.icon-search').on('click',function () {
       $(this).parents('.form-search').addClass('active');
       $('.form-search input').focus();
   });

   //show menu
    $('.navbar-toggler').on('click',function () {
       $('.navbar-collapse').addClass('is-show');
    });
    $('.navbar-collapse .close').on('click',function () {
        $('.navbar-collapse').removeClass('is-show');
    });

    // active navbar of page current
    var urlcurrent = window.location.href;
    $(".navbar-nav li a[href$='"+urlcurrent+"']").addClass('active');

    // effect navbar
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('header').addClass('scroll');
        }else{
            $('header').removeClass('scroll');
        }
    });
    $(document).mouseup(function(e){
        var container = $(".form-search");
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            $('.form-search').removeClass('active');
        }
    });

    //show text intro
    var width_device = $(window).width();
    if(width_device < 768){
        $('.intro_content article').addClass('txtmore');
    }

    var showChar = 300;
    var ellipsestext = "...";
    var moretext = "Xem thêm";

    $('.txtmore').each(function () {
        var content = $(this).html();
        if (content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

            $(this).html(html);
        }
    });
    $(".morelink").click(function () {
        if ($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html('');
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});